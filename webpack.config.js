const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = env => {
  return {
    entry: "./src/index",
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "bundle.js"
    },
    module:  {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader"
        },
        {
          test: /\.(sa|sc|c)ss$/,
          // Set loaders to transform files.
          // Loaders are applying from right to left(!)
          // The first loader will be applied after others
          use: [
            // creates style nodes from JS strings
            // disable mini css extract on development mode because webpack hmr doesn"t work with static css file
            env.NODE_ENV === "production" ? MiniCssExtractPlugin.loader : "style-loader",
            // This loader resolves url() and @imports inside CSS
            {
              loader: "css-loader",
              options: {
                modules: {
                  localIdentName: "[name]__[local]"
                },
                importLoaders: 1
              }
            },
            // Then we apply postCSS fixes like autoprefixer and minifying, translates CSS into CommonJS
            "postcss-loader",
            // First, compiles Sass to CSS, using Node Sass by default
            "sass-loader"
          ]
        },
        {
          // Now we apply rule for images
          test: /\.(png|jpe?g|gif|svg)$/,
          use: [
            {
              // Using file-loader for these files
              loader: "file-loader",

              // In options we can set different things like format
              // and directory to save
              options: {
                outputPath: "images"
              }
            }
          ]
        },
        {
          // Apply rule for fonts files
          test: /\.(woff|woff2|ttf|otf|eot)$/,
          use: [
            {
              // Using file-loader too
              loader: "file-loader",
              options: {
                outputPath: "fonts"
              }
            }
          ]
        }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, "public/index.html"),
        favicon: path.resolve(__dirname, "public/favicon.ico")
      }),
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // all options are optional
        filename: "bundle.css",
        ignoreOrder: false, // Enable to remove warnings about conflicting order
      }),
      new webpack.HotModuleReplacementPlugin()
    ],
    optimization: {
      splitChunks: {
        chunks: "all"
      }
    },
    devServer: {
      contentBase: path.join(__dirname, "dist"),
      compress: true,
      hotOnly: true,
      host: "0.0.0.0",
      port: 3000
    }
  };
};
