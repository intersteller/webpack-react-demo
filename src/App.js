import React from "react";

import appCss from "./App.scss";

const App = () => {
  return <h1 className={appCss.h1}>Application Content</h1>;
};

export default App;
