import React from "react";
import { render } from "react-dom";

import App from "./App";
import "./index.scss";

const renderContainer = Component => {
  const root = document.getElementById("root");

  if (root) {
    render(<Component />, root);
  }
};

renderContainer(App);

if (process.env.NODE_ENV === "development" && module.hot) {
  module.hot.accept("./App", () => {
    const nextApp = require('./App').default;
    renderContainer(nextApp);
  });
}
